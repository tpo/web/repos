# Web repositories

This repository is really a "meta-repository", which holds a list of
repositories of interest to the web team. It is similar to the
[repos](https://gitlab.torproject.org/tpo/tpa/repos) project of the TPA team.

It uses [myrepos](https://myrepos.branchable.com/), a shell script which allows you to keep multiple
repositories up to date in a somewhat lightweight manner.

## Installation

To deploy this, clone this repository somewhere (say `~/src/tor`) and
run `mr update` from it:

    sudo apt install myrepos
    git clone git@gitlab.torproject.org:tpo/web/repos.git ~/src/tor
    cd ~/src/tor
    echo $PWD/.mrconfig >> ~/.mrtrust
    mr update

If you want to connect it with your existing configuration, add the
`.mrconfig` file to your `.mrtrust` configuration file and register it
to your normal `.mrconfig`. This should get you started:

    cd ~
    mr register ~/src/tor
    mr config src/tor chain=true

## Security notes

The provided configuration assumes you *do* have SSH access to some
private repositories, although most public repositories are cloned
over HTTPS. Therefore, by default, this configuration trusts the [CA
cartel][] for transport authentication.

[CA cartel]: https://en.wikipedia.org/wiki/Certificate_authority

It also implies that pushing to those remotes will be impossible,
except if you add this to your `~/.gitconfig` to change the remote to
SSH automatically:

    # TorProject.org
    [url "ssh://git@gitlab.torproject.org/"]
        pushInsteadOf = https://gitlab.torproject.org/
    [url "ssh://git@git-rw.torproject.org/"]
        pushInsteadOf = https://git.torproject.org/

You can use a similar hack (with `insteadOf`) to force the use of SSH,
although that has been found to be (twice!) slower than using HTTPS.
